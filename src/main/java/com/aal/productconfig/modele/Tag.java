/**
 * 
 */
package com.aal.productconfig.modele;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author algas
 *
 */
@Entity
@Table(name = "Tag")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tag {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "id")
	private String id;
	@Column(name = "name")
	private String  name;
	@Column(name = "category")
	private String  category;
	@Column(name = "imageSrc")
	private String imageSrc;
	@ManyToOne(fetch = FetchType.LAZY)

//	 @JoinTable(name = "product_tag", 
//		joinColumns = {
//    @JoinColumn(name = "product_id") }, 
//		inverseJoinColumns = {
//    @JoinColumn(name = "tag_id") })
	//@JoinColumn(name="id", referencedColumnName = "id", insertable = false, updatable = false) 
	//@JsonBackReference
	private Product product;
	
}
