/**
 * 
 */
package com.aal.productconfig.modele;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author algas
 *
 */
@Entity
@Table(name = "Product")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product {

	@Id
	@GeneratedValue(generator = "uuid")
	
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "id")
	private String id;
	@Column(name = "name")
	private String name;
	@Column(name = "price")
	private double price;
	@Column(name = "description")
	private String description;
	 @JoinTable(name = "product_image", 
	    		joinColumns = {
	            @JoinColumn(name = "product_id") }, 
	    		inverseJoinColumns = {
	            @JoinColumn(name = "image_id") })
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonManagedReference
	private Set<Image> images = new HashSet<>();
	 
	 //@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	// @OneToMany(targetEntity=Product.class, mappedBy="Tag",cascade=CascadeType.ALL, fetch = FetchType.EAGER,orphanRemoval = true)    
	 @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)

	 // @JsonManagedReference
//	 @JoinTable(name = "product_tag", 
//		joinColumns = {
//     @JoinColumn(name = "product_id") }, 
//		inverseJoinColumns = {
//     @JoinColumn(name = "tag_id") })
	 private List<Tag> tags = new ArrayList<>();
}
