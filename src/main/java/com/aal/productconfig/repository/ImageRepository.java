package com.aal.productconfig.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aal.productconfig.modele.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, String>{
	public boolean existsByName(String name);
}