package com.aal.productconfig.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aal.productconfig.modele.Tag;

public interface TagRepository extends JpaRepository<Tag, String> {

}
