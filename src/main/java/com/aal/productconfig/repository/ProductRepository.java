/**
 * 
 */
package com.aal.productconfig.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aal.productconfig.modele.Product;

/**
 * @author algas
 *
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

}
