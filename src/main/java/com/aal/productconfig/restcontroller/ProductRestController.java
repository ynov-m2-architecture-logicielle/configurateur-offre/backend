/**
 * 
 */
package com.aal.productconfig.restcontroller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aal.productconfig.request.ProductRequestDto;
import com.aal.productconfig.service.ProductService;

/**
 * @author algas
 *
 */
@CrossOrigin(originPatterns = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/product-config")
public class ProductRestController {

	private ProductService productService;
	private Logger logger = LoggerFactory.getLogger(ProductRestController.class);
	
	/**
	 * @param productService
	 */
	@Autowired
	public ProductRestController(ProductService productService) {
		super();
		this.productService = productService;
	}
	@GetMapping("/product/{id}")
	ResponseEntity<?> getProduct(@PathVariable String id) {
		return new ResponseEntity<>(productService.getProduct(id),HttpStatus.OK);
	}
	@GetMapping("/products")
	public ResponseEntity<?> allProduct() {
		return new ResponseEntity<>(productService.allProduct(),HttpStatus.OK);
	}
	@PostMapping("/save")
	ResponseEntity<?> save(@RequestBody ProductRequestDto productRequestDto) {
		return new ResponseEntity<>(productService.save(productRequestDto),HttpStatus.OK);
	}
	@PutMapping("/update")
	ResponseEntity<?> update(@RequestBody ProductRequestDto productRequestDto,@RequestParam String id) {
		return new ResponseEntity<>(productService.update(productRequestDto,id),HttpStatus.OK);
	}
	@PutMapping("/delete")
	ResponseEntity<?> delete(@RequestParam String id) {
		return new ResponseEntity<>(productService.delete(id),HttpStatus.OK);
	}
	/**
	 * @return the productService
	 */
	public ProductService getProductService() {
		return productService;
	}
	/**
	 * @param productService the productService to set
	 */
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
	
	
	
}
