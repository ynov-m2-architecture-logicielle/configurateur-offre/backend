/**
 * 
 */
package com.aal.productconfig.response;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.aal.productconfig.modele.Image;
import com.aal.productconfig.modele.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author algas
 *
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductResponseDto {

	private String id;
	private String name;
	private double price;
	private String description;
	private Set<Image> images = new HashSet<>();
	private List<Tag> tags = new ArrayList<>();
}
