/**
 * 
 */
package com.aal.productconfig.mapper;

import org.mapstruct.Mapper;

import com.aal.productconfig.modele.Product;
import com.aal.productconfig.request.ProductRequestDto;
import com.aal.productconfig.response.ProductResponseDto;

/**
 * @author algas
 *
 */
@Mapper(componentModel = "spring")
public interface ProductMapper {

	Product productRequestDtoToProduct(ProductRequestDto productRequestDto);
	ProductResponseDto productToProductResponseDto(Product product);
	
}
