/**
 * 
 */
package com.aal.productconfig.service;

import java.util.List;

import com.aal.productconfig.request.ProductRequestDto;
import com.aal.productconfig.response.ProductResponseDto;

/**
 * @author algas
 *
 */
public interface ProductService {

	public ProductResponseDto getProduct(String id);
	public List<ProductResponseDto> allProduct();
	public ProductResponseDto save(ProductRequestDto productRequestDto);
	public ProductResponseDto update(ProductRequestDto productRequestDto,String id);
	public String delete(String id);
}
