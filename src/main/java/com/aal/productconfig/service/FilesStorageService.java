package com.aal.productconfig.service;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.aal.productconfig.modele.Image;

public interface FilesStorageService {
  public void init();

  public Image save(MultipartFile file);

  public Resource load(String filename);

  public void deleteAll();

  public Stream<Path> loadAll();
  public void deleteFile(String id);
}
