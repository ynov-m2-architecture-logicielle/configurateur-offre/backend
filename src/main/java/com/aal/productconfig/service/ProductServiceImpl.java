/**
 * 
 */
package com.aal.productconfig.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aal.productconfig.mapper.ProductMapper;
import com.aal.productconfig.modele.Image;
import com.aal.productconfig.modele.Product;
import com.aal.productconfig.modele.Tag;
import com.aal.productconfig.repository.ImageRepository;
import com.aal.productconfig.repository.ProductRepository;
import com.aal.productconfig.repository.TagRepository;
import com.aal.productconfig.request.ProductRequestDto;
import com.aal.productconfig.response.ProductResponseDto;

/**
 * @author algas
 *
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	private ProductRepository productRepository;
	private ProductMapper productMapper;
	private ImageRepository imageRepository;
	private TagRepository tagRepository;
	
	/**
	 * @param productRepository
	 * @param productMapper
	 */
	@Autowired
	public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper,ImageRepository imageRepository,TagRepository tagRepository) {
		super();
		this.productRepository = productRepository;
		this.productMapper = productMapper;
		this.imageRepository = imageRepository;
		this.tagRepository = tagRepository;
	}

	@Override
	public ProductResponseDto getProduct(String id) {
		return productMapper.productToProductResponseDto(productRepository.findById(id).get());
	}

	@Override
	public List<ProductResponseDto> allProduct() {
		
		return productRepository.findAll().stream().map(product->productMapper.productToProductResponseDto(product))
				.collect(Collectors.toList());
	}

	@Override
	public ProductResponseDto save(ProductRequestDto productRequestDto) {
		Product productSaved = productMapper.productRequestDtoToProduct(productRequestDto);
		Set<String> strImages = productRequestDto.getImagesStr();
		Set<Image> images = productSaved.getImages();
//		Tag tag = new Tag();
//		tag.setName(productSaved.getName());
//		tag.setCategory(productRequestDto.getCategory());
		List<Tag> tags = productSaved.getTags();
		Tag tag = new Tag();
		tag.setCategory("red");
		tag.setName("tag"+productRequestDto.getName());
		tag.setImageSrc("tag"+productRequestDto.getName());
		tagRepository.save(tag);
		if(!strImages.isEmpty()) {
			strImages.forEach(image->{
				Image newImage = new Image();
				newImage.setUrl(productSaved.getName()+image);
				newImage.setName(productSaved.getName());
				images.add(newImage);
			});
			imageRepository.saveAll(images);
			
			productSaved.setImages(images);
			
		}
		productSaved.setTags(tags);
		Product newProduct = productRepository.save(productSaved);
		return productMapper.productToProductResponseDto(newProduct);
	}

	@Override
	public ProductResponseDto update(ProductRequestDto productRequestDto, String id) {
		Product productUpdated = productRepository.findById(id).get();
		//Image newImage = new Image();
		productUpdated.setDescription(productRequestDto.getDescription());
		productUpdated.setName(productRequestDto.getName());
		productUpdated.setPrice(productRequestDto.getPrice());
		Set<String> strImages = productRequestDto.getImagesStr();
		Set<Image> images = productUpdated.getImages();
		
		if(!strImages.isEmpty()) {
			strImages.forEach(image->{
				Image newImage = new Image();
				newImage.setUrl(productUpdated.getName()+image);
				newImage.setName(productUpdated.getName());
				images.add(newImage);
			});
			imageRepository.saveAll(images);
			//productSaved.setTag(tag);
			productUpdated.setImages(images);
			
		}
		return productMapper.productToProductResponseDto(productRepository.save(productUpdated));
	}

	@Override
	public String delete(String id) {
		if(productRepository.existsById(id)) {
			productRepository.deleteById(id);
			return "DELETED SUCCESSFULLY";
		}
		return "DELETED failed, product is not exists";
	}

	/**
	 * @return the productRepository
	 */
	public ProductRepository getProductRepository() {
		return productRepository;
	}

	/**
	 * @param productRepository the productRepository to set
	 */
	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	/**
	 * @return the productMapper
	 */
	public ProductMapper getProductMapper() {
		return productMapper;
	}

	/**
	 * @param productMapper the productMapper to set
	 */
	public void setProductMapper(ProductMapper productMapper) {
		this.productMapper = productMapper;
	}




}
