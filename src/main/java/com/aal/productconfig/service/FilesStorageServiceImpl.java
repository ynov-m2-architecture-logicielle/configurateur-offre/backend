package com.aal.productconfig.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.aal.productconfig.modele.Image;
import com.aal.productconfig.repository.ImageRepository;

@Service
public class FilesStorageServiceImpl implements FilesStorageService {

	private ImageRepository imageRepository;
	private Logger logger =  LoggerFactory.getLogger(FilesStorageServiceImpl.class);
	private final Path root = Paths.get("uploads");
	
	/**
	 * @param imageRepository
	 */
	@Autowired
	public FilesStorageServiceImpl(ImageRepository imageRepository) {
		super();
		this.imageRepository = imageRepository;
	}

	@Override
	public void init() {
		  try {
		      Files.createDirectory(root);
		    } catch (IOException e) {
		      throw new RuntimeException("Could not initialize folder for upload!");
		    }
		
	}

	@Override
	public Image save(MultipartFile file) {
		 Image fileInfo = null;
		 Image newImage = new Image();
	    try {
	    	fileInfo = new Image();
	    if(!imageRepository.existsByName(fileInfo.getName())) {
	    	Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
		      fileInfo.setName(file.getOriginalFilename());
		      fileInfo.setUrl(this.root.resolve(file.getOriginalFilename()).toString());
		      newImage =  imageRepository.save(fileInfo);
		      
	    }
//	    else {
//	    	logger.error("this file is already exists");
//	    	return 
//	    }
	    
	      
	    } catch (Exception e) {
	      throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
	     
	    }
	    return newImage;
		
	}

	@Override
	public Resource load(String filename) {
		try {
		      Path file = root.resolve(filename);
		      Resource resource = new UrlResource(file.toUri());
		
		      if (resource.exists() || resource.isReadable()) {
		        return resource;
		      } else {
		        throw new RuntimeException("Could not read the file!");
		      }
		    } catch (MalformedURLException e) {
		      throw new RuntimeException("Error: " + e.getMessage());
		    }
	}

	@Override
	public void deleteAll() {
		 FileSystemUtils.deleteRecursively(root.toFile());
		
	}

	@Override
	public Stream<Path> loadAll() {
		 try {
		      return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
		    } catch (IOException e) {
		      throw new RuntimeException("Could not load the files!");
		    }
	}

	@Override
	public void deleteFile(String id) {
		Image fileInfo = imageRepository.findById(id).get();
		Path file = root.resolve(fileInfo.getName());
	      
		try {
			FileSystemUtils.deleteRecursively(file);
			imageRepository.deleteById(id);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}

	

}