/**
 * 
 */
package com.aal.productconfig.request;

import java.util.HashSet;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author algas
 *
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequestDto {
	private String name;
	private double price;
	private String description;
	private String category;
	private Set<String> imagesStr = new HashSet<>();

}
